/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.println("Enter the number to check for Armstrong");
		int num=ob.nextInt();
		int checkno=num;
		int sum=0,temp=0;
		while(num>0)
		{
		    temp=num%10;
		    sum=sum+(temp*temp*temp);
		    num=num/10;
		}
		if(sum==checkno)
		{
		    System.out.println("The number is an Armstrong no.");
		}
		else
		System.out.println("Not an Armstrong number");
	}
}
