/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.println("Enter the range of prime numbers to check");
		int range=ob.nextInt();
		for(int i=1;i<=range;i++)
		{
		    boolean flag=true;
		    for(int j=2;j<=i/2;j++)
		    {
		        if(i%j==0)
		        {
		        flag=false;
		        break;
		        }
		    }
		    if(flag==true)
		    System.out.print(i+" ");
		}
		
		
	}
}
