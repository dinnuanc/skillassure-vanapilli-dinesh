/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in); //Scanner object to read user data input
		//input of the 
        int a = sc.nextInt();  
        System.out.println("Enter the second number:");  
        int b = sc.nextInt();  
        System.out.println("Enter the third number:");  
        int c = sc.nextInt();  
        //comparing a and b and storing the smallest number in a temp variable  
        int temp=a<b?a:b;  
        //comparing the temp variable with c and storing the result in the variable names smallest  
        int smallest=c<temp?c:temp;  
        //prints the smallest number  
        System.out.println("The smallest number is: "+smallest);  
	}
}
