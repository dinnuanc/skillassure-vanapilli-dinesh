/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.println("Enter the size of array");
		int num=ob.nextInt();
		System.out.println("Enter the array elements");
		int arr[]=new int[num];
		for(int i=0;i<num;i++)
		arr[i]=ob.nextInt();
		System.out.println("Enter the element you want to find");
		int findNo=ob.nextInt();
		int count=0;
	    for(int j=0;j<num;j++)
	    {
	        if(arr[j]==findNo)
	        {
	        System.out.println("Element is at position "+ (j+1));
	        count++;
	        }
	    }
	    if(count==0)
	    System.out.println("Element not found");
		
		
	}
}
