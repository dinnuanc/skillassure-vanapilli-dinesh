/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in); //Scanner object to read user data input
        int n1=0,n2=1,n3,i;
        System.out.println("Enter the range you want");
        int count=ob.nextInt();    
        System.out.print(n1+" "+n2);//printing 0 and 1    
        for(i=2;i<count;++i)//0 and 1 are already printed so i=2   
        {    
            n3=n1+n2;    
            System.out.print(n3+",");    
            n1=n2;    
            n2=n3;    
        }    
	}
}
