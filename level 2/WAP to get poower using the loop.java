/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.println("Enter the number");
		int num=ob.nextInt();
		System.out.println("Enter power");
		int power=ob.nextInt();
		int value=1;
		for(int i=0;i<power;i++)
		value=value*num;
		System.out.println("The value is "+ value);
	}
}
