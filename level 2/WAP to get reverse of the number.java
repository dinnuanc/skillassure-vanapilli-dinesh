/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.println("Enter the number you want to reverse");
		int num=ob.nextInt();
		int sum=0,temp=0;
		while(num>0)
		{
		    temp=num%10;
		    sum=sum*10+temp;
		    num=num/10;
		}
		System.out.println("the reverse of the number is "+ sum);
		
		
	}
}
