/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.println("Enter the character to check");
		char ch=ob.next().charAt(0);
		switch(ch)
		{
		    case 'a':
		        System.out.println("This is a vowel");
		        break;
		    case 'o':
		        System.out.println("This is a vowel");
		        break;
		    case 'i':
		        System.out.println("This is a vowel");
		        break;
		    case 'e':
		        System.out.println("This is a vowel");
		        break;
		    case 'u':
		        System.out.println("This is a vowel");
		        break;
		    case 'A':
		        System.out.println("This is a vowel");
		        break;
		    case 'E':
		        System.out.println("This is a vowel");
		        break;
		    case 'I':
		        System.out.println("This is a vowel");
		        break;
		    case 'O':
		        System.out.println("This is a vowel");
		        break;
		    case 'U':
		        System.out.println("This is a vowel");
		        break;
		    default:
		        System.out.println("This is not a vowel");
		        break;
		}
		
		
	}
}
