/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in); //Scanner object to read user data input
		//input of the number
		System.out.println("Enter the number");
		int num=ob.nextInt();
		int sum=1;
		int sign=1;
		for(int i=1;i<=num;i++)
		{
		    System.out.print(sum*sign+",");
		    sum=sum+(i*i);
		    sign=sign*-1;
		}
	}
}
