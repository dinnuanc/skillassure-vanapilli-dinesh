/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.print("Enter employee name:");
		String name=ob.nextLine();
		System.out.print("Enter employee id:");
		long empid=ob.nextLong();
		System.out.print("Enter basic salary:");
		long basic=ob.nextLong();
		System.out.print("Enter special allowances:");
		long spallowances=ob.nextLong();
		System.out.print("Enter percentage of bonuses");
		float bonus=ob.nextFloat();
		System.out.print("Enter monthly tax saving investments");
		long taxsav=ob.nextLong();
		long gross_sal=basic+spallowances+taxsav;
		long annual_sal=gross_sal*12;
		double total_annual_sal=annual_sal+((bonus/100)*annual_sal);
		long tax_amount=0;
		tax_amount=(taxsav*12)-150000;
		if(tax_amount<0){
		    tax_amount=0;
		}
		double taxable_amount=total_annual_sal-tax_amount;
		double annual_net=0;
		if(annual_sal<=250000)
		{
		    annual_net=taxable_amount;
		}
		else if(annual_sal>250000&&annual_sal<=500000)
		{
		    annual_net=taxable_amount-(0.05*taxable_amount);
		}
		else if(annual_sal>500000&&annual_sal<=1000000)
		{
		    annual_net=taxable_amount-(0.2*taxable_amount);
		}
		else{
		    annual_net=taxable_amount-(0.3*taxable_amount);
		}
		System.out.print(annual_net);
	}
}
