/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;
public class Main
{
	public static void main(String[] args) {
		Scanner ob=new Scanner(System.in);
		System.out.println("Enter the rows and columns of the 2D array");
		int rows=ob.nextInt();
		int columns=ob.nextInt();
		int arr[][]=new int[rows][columns];
		System.out.println("Enter the elements of the 2D array");
		for(int i=0;i<rows;i++)
		{
		    for(int j=0;j<columns;j++)
		    arr[i][j]=ob.nextInt();
		}
		int arr2[][]=new int[columns][rows];
		for(int k=0;k<columns;k++)
		{
		    for(int l=0;l<rows;l++)
		    arr2[k][l]=arr[l][k];
		}
		for(int o=0;o<columns;o++)
		{
		    for(int p=0;p<rows;p++)
		    System.out.print(arr2[o][p]+" ");
		    System.out.println();
		}
		
	}
}
